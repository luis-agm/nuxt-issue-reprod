const defaultTheme = require("tailwindcss/defaultTheme")
const colors = require("tailwindcss/colors")

module.exports = {
  mode: "jit",
  theme: {
    screens: {
      xs: "320px",
      ...defaultTheme.screens,
    },
    extend: {
      height: (theme) => ({
        "screen/2": "50vh",
        "screen/3": "calc(100vh / 3)",
        "screen/4": "calc(100vh / 4)",
        "screen/5": "calc(100vh / 5)",
      }),
    },
    colors: {
      "light-gray": "#757575",
      "dark-gray": colors.neutral["800"],
      "darker-gray": colors.neutral["900"],
      danger: colors.red[500],
      "danger-dark": colors.red[800],
      warn: colors.yellow[300],
      "warn-dark": colors.yellow[500],
      white: colors.white,
      black: colors.black,
      neutral: colors.neutral,
      transparent: "transparent",
    },
  },
  safelist: ["w-2.5", "w-3.5", "w-4"],
  daisyui: {
    themes: [
      {
        mytheme: {
          primary: "#20BF55",
          secondary: "#0B401C",
          accent: "#18CCBA",
          neutral: "#2B2A3C",
          "base-100": colors.neutral[800],
          info: "#3454D5",
          success: "#69E2D8",
          warning: "#EFB539",
          error: "#E92566",
        },
      },
    ],
  },
  variants: {},
  plugins: [require("@tailwindcss/typography"), require("daisyui")],
}
