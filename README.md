# Basic setup with Nuxt 3 and TailwindCSS

Includes Nuxt modules for Google Fonts and Images.

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out the [documentation](https://v3.nuxtjs.org/).
