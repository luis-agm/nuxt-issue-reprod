export const debounce = (func, wait, immediate) => {
  let timeout
  return function () {
    const context = this
    const args = arguments
    clearTimeout(timeout)
    timeout = setTimeout(function () {
      timeout = null
      if (!immediate) func.apply(context, args)
    }, wait)
    if (immediate && !timeout) func.apply(context, args)
  }
}

export const throttle = (func, timeFrame) => {
  var lastTime = 0
  return function (...args) {
    var now = new Date()
    if (now - lastTime >= timeFrame) {
      func(...args)
      lastTime = now
    }
  }
}

export function nextStep(A, B, stepSize) {
  // Calculate the distance between A and B
  const distance = Math.sqrt((B[0] - A[0]) ** 2 + (B[1] - A[1]) ** 2)

  // If the distance is less than the step size, return B
  if (distance <= stepSize) {
    return B
  }

  // Calculate the x and y components of the direction vector
  const dx = (B[0] - A[0]) / distance
  const dy = (B[1] - A[1]) / distance

  // Calculate the next point using the direction vector and step size
  const nextX = A[0] + stepSize * dx
  const nextY = A[1] + stepSize * dy

  return [nextX, nextY]
}

export const getCookieByName = (name, cookieStore) => {
  const match = cookieStore.match(new RegExp("(^| )" + name + "=([^;]+)"))
  if (match) return match[2]
}
