export default defineNuxtConfig({
  components: ["./components"],

  modules: [
    "@nuxt/content",
    "@nuxtjs/tailwindcss",
    "@nuxtjs/google-fonts",
    "@nuxt/image-edge",
  ],

  build: {
    analyze: true,
  },

  typescript: {
    shim: false,
  },
  vite: {
    plugins: [],
  },
})
